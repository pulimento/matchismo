//
//  PT1PlayingCard.m
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1PlayingCard.h"

@implementation PT1PlayingCard

- (NSString *)contents{
    NSArray *rankStrings = [PT1PlayingCard rankStrings];
    NSUInteger rank = self.rank;
    NSString *suit = self.suit;    
    return [rankStrings[rank] stringByAppendingString:suit];
}

- (NSString *)description{
    return [self contents];
}

@synthesize  suit = _suit; // because we provide setter AND getter

+ (NSArray *)validSuits{
    // creates an array on the fly
    // create strings in that manner on the fly is new in ios6
    return @[@"♠",@"♥",@"♦",@"♣"];
}

- (void)setSuit:(NSString *)suit{
    if([[PT1PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

- (NSString *)suit{
    return _suit ? _suit : @"?";
}

+ (NSArray *)rankStrings{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+ (NSUInteger)maxRank{
    return [self rankStrings].count - 1;
}

- (void)setRank:(NSUInteger)rank{
    if(rank <= [PT1PlayingCard maxRank]){
        _rank = rank;
    }
}

- (int)multipleMatch:(NSArray *)otherCards{
    
    int score = 0;
    
    if([otherCards count] == 1){
        // The method lastobject never throws an indexoutofbounds error, if the array is empty (which is not the case here), simply returns nil
        PT1PlayingCard *otherCard = [otherCards lastObject];
        if([otherCard isKindOfClass:[PT1PlayingCard class]]){
            PT1PlayingCard *otherPlayingCard = (PT1PlayingCard *)otherCard;// casting not needed, is for helping other people reading my code
            if([otherPlayingCard.suit isEqualToString:self.suit]){
                score = 1;
            }else if (otherPlayingCard.rank == self.rank){
                score = 4;
            }
        }
        
    }
    return score;
}


@end
