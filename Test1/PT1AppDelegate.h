//
//  PT1AppDelegate.h
//  Test1
//
//  Created by Javi Pulido on 13/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PT1AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
