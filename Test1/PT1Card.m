//
//  PT1Card.m
//  Test1
//
//  Created by Javi Pulido on 13/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1Card.h"

@interface PT1Card()

// private methods going here
// really don't want private methods yet, i need properties right now (getters and setters)

@end

@implementation PT1Card

// implementation of a method declared previously in the header file
// receives a pointer to a card, returns an integer
- (int)match:(PT1Card *)card{
    int score = 0;
    
    // here in card.contents we're calling the "generated getter" from property declared in the header
    // form of passing a message to an object in objC : open brackets, the object that i want to send the message to, the message, and then its arguments, and close square bracket
    if([card.contents isEqualToString:self.contents]) {
        score = 1;
    }
    
    return score;
}

- (int) multipleMatch:(NSArray *)otherCards{
    int score = 0;
    
    for(PT1Card *card in otherCards){
        if([card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    
    return score;
}

@end
