//
//  PT1Deck.h
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT1Card.h"

@interface PT1Deck : NSObject

- (void) addCard:(PT1Card *)card atTop:(BOOL)atTop;

- (PT1Card *)drawRandomCard;

@end
