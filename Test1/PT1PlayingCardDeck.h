//
//  PT1PlayingCardDeck.h
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT1Deck.h"

@interface PT1PlayingCardDeck : PT1Deck

@end
