//
//  PT1ViewController.m
//  Test1
//
//  Created by Javi Pulido on 13/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1ViewController.h"
#import "PT1PlayingCardDeck.h"
#import "PT1CardMatchingGame.h"


@interface PT1ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) PT1CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastFlipsLabel;
@property (weak, nonatomic) IBOutlet UISlider *historySlider;

- (IBAction)newGameButton:(UIButton *)sender;

@end

@implementation PT1ViewController


- (PT1CardMatchingGame *)game{
    if(!_game) _game = [[PT1CardMatchingGame alloc]initWithCardCount:[self.cardButtons count] usingDeck:[[PT1PlayingCardDeck alloc]init]];
    return _game;
}

- (void) setCardButtons:(NSArray *)cardButtons{
    _cardButtons = cardButtons;
    UIImage *logo = [UIImage imageNamed:@"gdtLogo.png"];
    UIImage *trans = [UIImage imageNamed:@"transparent.png"];
    for(UIButton *btn in cardButtons){
        [btn setImage:logo forState:UIControlStateNormal];
        [btn setImage:trans forState:UIControlStateSelected];
        [btn setImage:trans forState:UIControlStateSelected|UIControlStateDisabled];
    }
    [self updateUI];
}
- (IBAction)historySliding:(UISlider *)sender {
    if(!(sender.maximumValue <= 0 || sender.value == INT_MAX || self.game.lastFlips.count == 0)){
        self.lastFlipsLabel.text = self.game.lastFlips[(int)sender.value];
        self.lastFlipsLabel.alpha = (sender.maximumValue != sender.value && sender.maximumValue >=2) ? .3 : 1;
    }
}

- (void)updateUI{
    // maintains the view in sync with the model. this is a commonly used method
    for(UIButton *cardButton in self.cardButtons){
        PT1Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        // if i don't use the next line, when the button were still selected, it will use the default text because it's disabled too
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled];
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.unplayable;
        cardButton.alpha = (card.unplayable ? .3 : 1);
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Puntos: %d", self.game.score];
    [self.historySlider setMaximumValue:self.game.lastFlips.count - 1];
    [self.historySlider setValue:self.game.lastFlips.count - 1];
    [self.historySlider setContinuous:YES];
    self.lastFlipsLabel.text = [self.game.lastFlips lastObject];
}


- (void)setFlipCount:(int)flipCount{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UIButton *)sender {
    [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    self.flipCount++;
    [self updateUI];
}

- (IBAction)newGameButton:(UIButton *)sender {
    // reset flip count
    [self setFlipCount:0];
    // new game with new deck
    PT1CardMatchingGame *newCardGame = [[PT1CardMatchingGame alloc]initWithCardCount:[self.cardButtons count] usingDeck:[[PT1PlayingCardDeck alloc]init]];
    [self setGame:newCardGame];
    // finally, update the view
    [self updateUI];
}
@end