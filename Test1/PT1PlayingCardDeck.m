//
//  PT1PlayingCardDeck.m
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1PlayingCardDeck.h"
#import "PT1PlayingCard.h"

@implementation PT1PlayingCardDeck

- (id)init{
    
    self = [super init];
    
    if(self){
        for(NSString *suit in [PT1PlayingCard validSuits]) {
            for(NSUInteger rank = 1; rank <= [PT1PlayingCard maxRank]; rank++) {
                PT1PlayingCard *card =[[PT1PlayingCard alloc]init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:YES];
            }
        }
    }
    return self;
}

@end
