//
//  PT1Deck.m
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1Deck.h"
#import "PT1Card.h"

@interface PT1Deck()

// of course, is strong because if it is private, no one else will point at it
@property(strong, nonatomic) NSMutableArray *cards; //of Card, private property!!

@end

@implementation PT1Deck

// Not overriding, implements the getter
// When you implements the getter, compiler doesn't autoimplement it
- (NSMutableArray *)cards{
    if(!_cards) _cards = [[NSMutableArray alloc]init];
    return _cards;
}

- (void)addCard:(PT1Card *)card atTop:(BOOL)atTop{
    
    if(card){
        if(atTop){
            [self.cards insertObject:card atIndex:0];
        } else {
            [self.cards addObject:card];
        }
    }
    
}

- (PT1Card *)drawRandomCard{
    
    PT1Card *randomCard = nil;
    
    if(self.cards.count){        
        // unsigned integer, this is a c thing
        unsigned index = arc4random() % self.cards.count;
        // only can use array[i] in ios6, before must use self.cards getObjectForIndex
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    
    return randomCard;    
}

@end
