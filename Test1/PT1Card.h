//
//  PT1Card.h
//  Test1
//
//  Created by Javi Pulido on 13/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PT1Card : NSObject

// getter and setter, pointer to a nsstring object
// strong : maintain on the heap as long as i point into it
// weak : only keep in the heap as long as someone else points to it strongly
// nonatomic : these getters and setters aren't thread-safe
// atomic : default
@property (strong, nonatomic) NSString *contents;

// these aren't pointers, simply bool objects, so no strong/weak
// simply renaming getter here, not doing any more
@property (nonatomic, getter=isFaceUp) BOOL faceUp;
@property (nonatomic, getter=isUnplayable) BOOL unplayable;

- (int) match:(PT1Card *) card;

- (int) multipleMatch:(NSArray *) otherCards;

@end
