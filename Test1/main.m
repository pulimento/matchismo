//
//  main.m
//  Test1
//
//  Created by Javi Pulido on 13/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PT1AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PT1AppDelegate class]));
    }
}