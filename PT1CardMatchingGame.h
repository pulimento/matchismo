//
//  PT1CardMatchingGame.h
//  Test1
//
//  Created by Javi Pulido on 15/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT1Card.h"
#import "PT1Deck.h"

@interface PT1CardMatchingGame : NSObject

// designated initializer
// it's not an implicit initializer
- (id)initWithCardCount: (NSUInteger)count
              usingDeck:(PT1Deck *)deck;

- (void)flipCardAtIndex:(NSUInteger)index;

- (PT1Card *)cardAtIndex:(NSUInteger)index;

// readonly means that it's only a getter for that
// remember that this is public, we don't want anyone to change our score
@property(readonly, nonatomic) int score;
@property(readonly, nonatomic) NSString *lastFlip;
@property(nonatomic) NSMutableArray *lastFlips;

@end
