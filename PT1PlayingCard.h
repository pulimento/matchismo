//
//  PT1PlayingCard.h
//  Test1
//
//  Created by Javi Pulido on 14/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT1Card.h"

@interface PT1PlayingCard : PT1Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSUInteger)maxRank;
+ (NSArray *)validSuits;



@end
