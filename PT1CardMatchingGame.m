//
//  PT1CardMatchingGame.m
//  Test1
//
//  Created by Javi Pulido on 15/01/14.
//  Copyright (c) 2014 Javi Pulido. All rights reserved.
//

#import "PT1CardMatchingGame.h"

@interface PT1CardMatchingGame()

//this score have got a getter and a setter, but only for private use, instead of read-only one declared in the header
@property (readwrite, nonatomic) int score;
@property (strong, nonatomic) NSMutableArray *cards; //of PT1Card
@property (readwrite, nonatomic) NSString *lastFlip;

@end

@implementation PT1CardMatchingGame

- (NSMutableArray *)cards{
    if(!_cards) _cards = [[NSMutableArray alloc]init];
    return _cards;
}

#define MATCH_BONUS 4
#define MISMATCH_PENALTY 2
#define FLIP_COST 1

- (void)flipCardAtIndex:(NSUInteger)index{
    PT1Card *card = [self cardAtIndex:index];
    NSString *currentLastFlip = nil;
    
    if(card && !card.unplayable){
        if(!card.isFaceUp){
            BOOL found = NO;
            for(PT1Card *otherCard in self.cards){
                if(otherCard.isFaceUp && !otherCard.unplayable){
                    found = YES;
                    int matchScore = [card multipleMatch:@[otherCard]];
                    if(matchScore){
                        card.unplayable = YES;
                        otherCard.unplayable = YES;
                        NSUInteger pointsToAdd = matchScore * MATCH_BONUS;
                        self.score += pointsToAdd;
                        currentLastFlip = [[NSString alloc]initWithFormat: @"\xF0\x9F\x98\x84 %@ coincide con %@, +%d puntos",card.contents,otherCard.contents,pointsToAdd];
                    } else {
                        otherCard.faceUp = NO;
                        self.score -= MISMATCH_PENALTY;                        
                        currentLastFlip = [[NSString alloc]initWithFormat: @"\xF0\x9F\x98\x9E %@ no coincide con %@, -%d puntos",card.contents,otherCard.contents,MISMATCH_PENALTY];
                    }
                    break;
                }
            }
            self.score -= FLIP_COST;
            if(!found) currentLastFlip = [[NSString alloc]initWithFormat:@"Se ha girado %@",card.contents];
            [self setLastFlip:currentLastFlip];
            [self.lastFlips addObject:currentLastFlip];
        }
        card.faceUp = !card.isFaceUp;
    }
}

- (PT1Card *)cardAtIndex:(NSUInteger)index{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

- (id)initWithCardCount:(NSUInteger)count usingDeck:(PT1Deck *)deck{
    self = [super init];
    
    if(self){
        for(int i = 0;i < count; i++){
            PT1Card *card = [deck drawRandomCard];
            if(card){
                self.cards[i] = card;
            } else {
                // if we can't initialize the game, return nil
                self = nil;
                break;
            }
        }
    }    
    return self;
}

- (void) setLastFlip:(NSString *)lastFlip{
    if(!_lastFlip) _lastFlip = @"";
    _lastFlip = lastFlip;
}

// lazy-initialization getter for last flips
- (NSMutableArray *)lastFlips{
    if(!_lastFlips) _lastFlips = [[NSMutableArray alloc]init];
    return _lastFlips;
}

@end
